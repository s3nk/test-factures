CREATE TABLE ref_conditions_paiement
  (
     id                SERIAL PRIMARY KEY,
     code              VARCHAR(50) NOT NULL,
     libelle           VARCHAR(50) NOT NULL,
     date_creation     TIMESTAMP NOT NULL,
     date_modification TIMESTAMP NOT NULL,
     user_creation     VARCHAR(50) NOT NULL,
     user_modification VARCHAR(50) NOT NULL
  );

INSERT INTO ref_conditions_paiement
            (code,
             libelle,
             date_creation,
             date_modification,
             user_creation,
             user_modification)
VALUES      ('REGLEMENT_LIVRAISON',
             'Règlement à la livraison',
             Now(),
             Now(),
             'flyway',
             'flyway');

CREATE INDEX ON ref_conditions_paiement(code);

CREATE TABLE adresse
  (
     id                SERIAL PRIMARY KEY,
     raison_sociale    VARCHAR(200) NOT NULL,
     ligne             VARCHAR(200) NOT NULL,
     ville             VARCHAR(100) NOT NULL,
     code_postal       VARCHAR(10) NOT NULL,
     date_creation     TIMESTAMP NOT NULL,
     date_modification TIMESTAMP NOT NULL,
     user_creation     VARCHAR(50) NOT NULL,
     user_modification VARCHAR(50) NOT NULL
  );

CREATE TABLE client
  (
     code              VARCHAR(15) PRIMARY KEY,
     id_adresse        INTEGER NOT NULL,
     date_creation     TIMESTAMP NOT NULL,
     date_modification TIMESTAMP NOT NULL,
     user_creation     VARCHAR(50) NOT NULL,
     user_modification VARCHAR(50) NOT NULL
  );

CREATE TABLE coordonnees_bancaires
  (
     id                SERIAL PRIMARY KEY,
     domiciliation     VARCHAR(50) NOT NULL,
     proprietaire      VARCHAR(200) NOT NULL,
     iban              VARCHAR(35) NOT NULL,
     bic               VARCHAR(11) NOT NULL,
     date_creation     TIMESTAMP NOT NULL,
     date_modification TIMESTAMP NOT NULL,
     user_creation     VARCHAR(50) NOT NULL,
     user_modification VARCHAR(50) NOT NULL
  );

CREATE TABLE designation
  (
     id                SERIAL PRIMARY KEY,
     libelle           VARCHAR(100) NOT NULL,
     prix_unitaire_ht  DECIMAL(12, 2) NOT NULL,
     date_creation     TIMESTAMP NOT NULL,
     date_modification TIMESTAMP NOT NULL,
     user_creation     VARCHAR(50) NOT NULL,
     user_modification VARCHAR(50) NOT NULL
  );

CREATE TABLE tva
  (
     id                SERIAL PRIMARY KEY,
     taux              DECIMAL(12, 2) NOT NULL,
     libelle           VARCHAR(100) NOT NULL,
     date_creation     TIMESTAMP NOT NULL,
     date_modification TIMESTAMP NOT NULL,
     user_creation     VARCHAR(50) NOT NULL,
     user_modification VARCHAR(50) NOT NULL
  );

CREATE TABLE designation_tva
  (
     id_designation    INTEGER NOT NULL,
     id_tva            INTEGER NOT NULL,
     date_debut        TIMESTAMP NOT NULL,
     date_fin          TIMESTAMP,
     date_creation     TIMESTAMP NOT NULL,
     date_modification TIMESTAMP NOT NULL,
     user_creation     VARCHAR(50) NOT NULL,
     user_modification VARCHAR(50) NOT NULL,
     FOREIGN KEY (id_designation) REFERENCES designation (id),
     FOREIGN KEY (id_tva) REFERENCES tva (id),
     PRIMARY KEY (id_designation, id_tva)
  );

CREATE TABLE structure
  (
     id                       SERIAL PRIMARY KEY,
     libelle                  VARCHAR(200) NOT NULL,
     url_logo                 VARCHAR(200) NOT NULL,
     id_adresse               INTEGER NOT NULL,
     id_coordonnees_bancaires INTEGER NOT NULL,
     date_creation            TIMESTAMP NOT NULL,
     date_modification        TIMESTAMP NOT NULL,
     user_creation            VARCHAR(50) NOT NULL,
     user_modification        VARCHAR(50) NOT NULL,
     FOREIGN KEY (id_adresse) REFERENCES adresse (id),
     FOREIGN KEY (id_coordonnees_bancaires) REFERENCES coordonnees_bancaires (id
     )
  );

CREATE TABLE facture
  (
     reference                  VARCHAR(10) PRIMARY KEY,
     date_facturation           TIMESTAMP NOT NULL,
     date_echeance              TIMESTAMP NOT NULL,
     id_adresse                 INTEGER NOT NULL,
     code_client                VARCHAR(15) NOT NULL,
     id_structure               INTEGER NOT NULL,
     id_ref_conditions_paiement INTEGER NOT NULL,
     date_creation              TIMESTAMP NOT NULL,
     date_modification          TIMESTAMP NOT NULL,
     user_creation              VARCHAR(50) NOT NULL,
     user_modification          VARCHAR(50) NOT NULL,
     FOREIGN KEY (id_adresse) REFERENCES adresse (id),
     FOREIGN KEY (code_client) REFERENCES client (code),
     FOREIGN KEY (id_structure) REFERENCES structure (id),
     FOREIGN KEY (id_ref_conditions_paiement) REFERENCES ref_conditions_paiement
     (id)
  );

CREATE TABLE facture_designation
  (
     reference_facture    VARCHAR(10) NOT NULL,
     id_designation       INTEGER NOT NULL,
     libelle_designation  VARCHAR(100) NOT NULL,
     prix_unitaire_ht     DECIMAL(12, 2) NOT NULL,
     id_tva               INTEGER NOT NULL,
     montant_unitaire_tva DECIMAL(12, 2) NOT NULL,
     quantite             DECIMAL(12, 2) NOT NULL,
     date_creation        TIMESTAMP NOT NULL,
     date_modification    TIMESTAMP NOT NULL,
     user_creation        VARCHAR(50) NOT NULL,
     user_modification    VARCHAR(50) NOT NULL,
     FOREIGN KEY (reference_facture) REFERENCES facture (reference),
     FOREIGN KEY (id_designation) REFERENCES designation (id),
     FOREIGN KEY (id_tva) REFERENCES tva (id),
     PRIMARY KEY (reference_facture, id_designation)
  );