package com.test.factures.controller;

import com.test.factures.exception.NotFoundException;
import com.test.factures.mapper.StructureDtoMapper;
import com.test.factures.model.dto.StructureCreationDto;
import com.test.factures.model.dto.StructureDto;
import com.test.factures.service.business.StructureService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "StructureController", description = "API dédiée aux structures")
@RestController
@RequiredArgsConstructor
public class StructureController {
    private final StructureService structureService;
    private final StructureDtoMapper structureDtoMapper;

    @GetMapping(value = "/api/v1/structures")
    public List<StructureDto> getAllStructures() {
        return structureDtoMapper.toDestination(structureService.getAllStructures());
    }

    @GetMapping(value = "/api/v1/structures/{idStructure}")
    public StructureDto getStructure(
            @PathVariable(name = "idStructure") Integer idStructure
    ) throws NotFoundException {
        return structureDtoMapper.toDestination(structureService.getStructure(idStructure));
    }

    @PostMapping(value = "/api/v1/structures")
    public StructureDto creerStructure(
            @Valid @RequestBody StructureCreationDto donneesStructure
    ) throws NotFoundException {
        return structureDtoMapper.toDestination(structureService.creerStructure(donneesStructure));
    }
}