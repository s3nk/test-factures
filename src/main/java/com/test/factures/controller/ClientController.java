package com.test.factures.controller;

import com.test.factures.exception.FunctionalException;
import com.test.factures.exception.NotFoundException;
import com.test.factures.mapper.AdresseDtoMapper;
import com.test.factures.mapper.ClientDtoMapper;
import com.test.factures.model.dto.AdresseDto;
import com.test.factures.model.dto.AdresseModificationDto;
import com.test.factures.model.dto.ClientCreationDto;
import com.test.factures.model.dto.ClientDto;
import com.test.factures.service.business.ClientService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "ClientController", description = "API dédiée aux clients")
@RestController
@RequiredArgsConstructor
public class ClientController {
    private final ClientService clientService;
    private final ClientDtoMapper clientDtoMapper;
    private final AdresseDtoMapper adresseDtoMapper;

    @GetMapping(value = "/api/v1/clients")
    public List<ClientDto> getAllClients() {
        return clientDtoMapper.toDestination(clientService.getAllClients());
    }

    @GetMapping(value = "/api/v1/clients/{codeClient}")
    public ClientDto getClient(
            @PathVariable(name = "codeClient") String codeClient
    ) throws NotFoundException {
        return clientDtoMapper.toDestination(clientService.getClient(codeClient));
    }

    @PostMapping(value = "/api/v1/clients")
    public ClientDto creerClient(
            @Valid @RequestBody ClientCreationDto donneesClient
    ) throws NotFoundException, FunctionalException {
        return clientDtoMapper.toDestination(clientService.creerClient(donneesClient));
    }

    @PatchMapping(value = "/api/v1/clients/{codeClient}/adresses/ACTUELLE")
    public AdresseDto modifierAdresseClient(
            @PathVariable(name = "codeClient") String codeClient,
            @Valid @RequestBody AdresseModificationDto donneesAdresse
    ) throws NotFoundException {
        return adresseDtoMapper.toDestination(clientService.modifierAdresseClient(codeClient, donneesAdresse));
    }
}