package com.test.factures.controller;

import com.test.factures.exception.NotFoundException;
import com.test.factures.mapper.TvaDtoMapper;
import com.test.factures.model.dto.TvaCreationDto;
import com.test.factures.model.dto.TvaDto;
import com.test.factures.service.business.TvaService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "TvaController", description = "API dédiée à la TVA")
@RestController
@RequiredArgsConstructor
public class TvaController {
    private final TvaService tvaService;
    private final TvaDtoMapper tvaDtoMapper;

    @GetMapping(value = "/api/v1/tva")
    public List<TvaDto> getAllTva() {
        return tvaDtoMapper.toDestination(tvaService.getAllTva());
    }

    @GetMapping(value = "/api/v1/tva/{idTva}")
    public TvaDto getTva(
            @PathVariable(name = "idTva") Integer idTva
    ) throws NotFoundException {
        return tvaDtoMapper.toDestination(tvaService.getTva(idTva));
    }

    @PostMapping(value = "/api/v1/tva")
    public TvaDto creerTva(
            @Valid @RequestBody TvaCreationDto donneesTva
    ) throws NotFoundException {
        return tvaDtoMapper.toDestination(tvaService.creerTva(donneesTva));
    }
}