package com.test.factures.controller;

import com.test.factures.exception.FunctionalException;
import com.test.factures.exception.NotFoundException;
import com.test.factures.exception.TechnicalException;
import com.test.factures.mapper.FactureDtoMapper;
import com.test.factures.model.dto.FactureCreationDto;
import com.test.factures.model.dto.FactureDto;
import com.test.factures.service.business.FactureService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "FactureController", description = "API dédiée aux factures")
@RestController
@RequiredArgsConstructor
public class FactureController {
    private final FactureService factureService;
    private final FactureDtoMapper factureDtoMapper;

    @GetMapping(value = "/api/v1/factures")
    public List<FactureDto> getAllFactures(
            @RequestParam(value = "codeClient", required = false) String codeClient
    ) {
        return factureDtoMapper.toDestination(factureService.getAllFactures(codeClient));
    }

    @GetMapping(value = "/api/v1/factures/{referenceFacture}")
    public FactureDto getFacture(
            @PathVariable(name = "referenceFacture") String referenceFacture
    ) throws NotFoundException {
        return factureDtoMapper.toDestination(factureService.getFacture(referenceFacture));
    }

    @PostMapping(value = "/api/v1/factures")
    public FactureDto creerFacture(
            @Valid @RequestBody FactureCreationDto donneesFacture
    ) throws NotFoundException, TechnicalException, FunctionalException {
        return factureDtoMapper.toDestination(factureService.creerFacture(donneesFacture));
    }
}