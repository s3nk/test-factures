package com.test.factures.controller;

import com.test.factures.exception.NotFoundException;
import com.test.factures.mapper.DesignationDtoMapper;
import com.test.factures.model.dto.DesignationCreationDto;
import com.test.factures.model.dto.DesignationDto;
import com.test.factures.model.dto.DesignationModificationDto;
import com.test.factures.service.business.DesignationService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "DesignationController", description = "API dédiée aux designations")
@RestController
@RequiredArgsConstructor
public class DesignationController {
    private final DesignationService designationService;
    private final DesignationDtoMapper designationDtoMapper;

    @GetMapping(value = "/api/v1/designations")
    public List<DesignationDto> getAllDesignations() {
        return designationDtoMapper.toDestination(designationService.getAllDesignations());
    }

    @GetMapping(value = "/api/v1/designations/{idDesignation}")
    public DesignationDto getDesignation(
            @PathVariable(name = "idDesignation") Integer idDesignation
    ) throws NotFoundException {
        return designationDtoMapper.toDestination(designationService.getDesignation(idDesignation));
    }

    @PostMapping(value = "/api/v1/designations")
    public DesignationDto creerDesignation(
            @Valid @RequestBody DesignationCreationDto donneesDesignation
    ) throws NotFoundException {
        return designationDtoMapper.toDestination(designationService.creerDesignation(donneesDesignation));
    }

    @PatchMapping(value = "/api/v1/designations/{idDesignation}")
    public DesignationDto modifierDesignation(
            @PathVariable(name = "idDesignation") Integer idDesignation,
            @Valid @RequestBody DesignationModificationDto donneesDesignation
    ) throws NotFoundException {
        return designationDtoMapper.toDestination(designationService.modifierDesignation(idDesignation, donneesDesignation));
    }
}