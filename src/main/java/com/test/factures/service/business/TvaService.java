package com.test.factures.service.business;

import com.test.factures.exception.NotFoundException;
import com.test.factures.model.dto.TvaCreationDto;
import com.test.factures.model.model.TvaModel;

import java.util.List;

public interface TvaService {
    List<TvaModel> getAllTva();

    TvaModel getTva(Integer idTva) throws NotFoundException;

    TvaModel creerTva(TvaCreationDto donneesTva);
}
