package com.test.factures.service.business;

import com.test.factures.exception.NotFoundException;
import com.test.factures.mapper.AdresseCreationDtoMapper;
import com.test.factures.mapper.CoordonneesBancairesCreationDtoMapper;
import com.test.factures.model.dto.StructureCreationDto;
import com.test.factures.model.model.AdresseModel;
import com.test.factures.model.model.CoordonneesBancairesModel;
import com.test.factures.model.model.StructureCreationModel;
import com.test.factures.model.model.StructureModel;
import com.test.factures.service.data.AdresseDataService;
import com.test.factures.service.data.CoordonneesBancairesDataService;
import com.test.factures.service.data.StructureDataService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StructureServiceImpl implements StructureService {
    private final StructureDataService structureDataService;
    private final AdresseDataService adresseDataService;
    private final AdresseCreationDtoMapper adresseCreationDtoMapper;
    private final CoordonneesBancairesDataService coordonneesBancairesDataService;
    private final CoordonneesBancairesCreationDtoMapper coordonneesBancairesCreationDtoMapper;

    @Override
    public List<StructureModel> getAllStructures() {
        return structureDataService.getAllStructures();
    }

    @Override
    public StructureModel getStructure(Integer idStructure) throws NotFoundException {
        return structureDataService.getStructure(idStructure);
    }

    @Transactional
    @Override
    public StructureModel creerStructure(StructureCreationDto donneesStructure) throws NotFoundException {
        AdresseModel adresseCree = adresseDataService.creerAdresse(
                adresseCreationDtoMapper.toDestination(donneesStructure.getAdresse())
        );
        CoordonneesBancairesModel coordonneesBancairesCrees = coordonneesBancairesDataService.creerCoordonneesBancaires(
                coordonneesBancairesCreationDtoMapper.toDestination(donneesStructure.getCoordonneesBancaires())
        );
        StructureCreationModel structureCreationModel = new StructureCreationModel(
                donneesStructure.getLibelle(),
                donneesStructure.getUrlLogo(),
                adresseCree.getId(),
                coordonneesBancairesCrees.getId()
        );
        return structureDataService.creerStructure(structureCreationModel);
    }
}
