package com.test.factures.service.business;

import com.test.factures.exception.NotFoundException;
import com.test.factures.mapper.TvaCreationDtoMapper;
import com.test.factures.model.dto.TvaCreationDto;
import com.test.factures.model.model.TvaModel;
import com.test.factures.service.data.TvaDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TvaServiceImpl implements TvaService {
    private final TvaDataService tvaDataService;
    private final TvaCreationDtoMapper tvaCreationDtoMapper;

    @Override
    public List<TvaModel> getAllTva() {
        return tvaDataService.getAllTva();
    }

    @Override
    public TvaModel getTva(Integer idTva) throws NotFoundException {
        return tvaDataService.getTva(idTva);
    }

    @Override
    public TvaModel creerTva(TvaCreationDto donneesTva) {
        return tvaDataService.creerTva(
                tvaCreationDtoMapper.toDestination(donneesTva)
        );
    }
}
