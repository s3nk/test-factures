package com.test.factures.service.business;

import com.test.factures.exception.NotFoundException;
import com.test.factures.mapper.DesignationCreationDtoMapper;
import com.test.factures.mapper.DesignationModificationDtoMapper;
import com.test.factures.model.dto.DesignationCreationDto;
import com.test.factures.model.dto.DesignationModificationDto;
import com.test.factures.model.model.DesignationModel;
import com.test.factures.service.data.DesignationDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DesignationServiceImpl implements DesignationService {
    private final DesignationDataService designationDataService;
    private final DesignationCreationDtoMapper designationCreationDtoMapper;
    private final DesignationModificationDtoMapper designationModificationDtoMapper;

    @Override
    public List<DesignationModel> getAllDesignations() {
        return designationDataService.getAllDesignations();
    }

    @Override
    public DesignationModel getDesignation(Integer idDesignation) throws NotFoundException {
        return designationDataService.getDesignation(idDesignation);
    }

    @Override
    public DesignationModel creerDesignation(DesignationCreationDto donneesDesignation) throws NotFoundException {
        return designationDataService.creerDesignation(designationCreationDtoMapper.toDestination(donneesDesignation));
    }

    @Override
    public DesignationModel modifierDesignation(Integer idDesignation, DesignationModificationDto donneesDesignation) throws NotFoundException {
        return designationDataService.modifierDesignation(idDesignation, designationModificationDtoMapper.toDestination(donneesDesignation));
    }
}
