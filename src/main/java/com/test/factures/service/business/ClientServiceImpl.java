package com.test.factures.service.business;

import com.test.factures.exception.FactureException;
import com.test.factures.exception.FunctionalException;
import com.test.factures.exception.NotFoundException;
import com.test.factures.mapper.AdresseCreationDtoMapper;
import com.test.factures.mapper.AdresseModificationDtoMapper;
import com.test.factures.model.dto.AdresseModificationDto;
import com.test.factures.model.dto.ClientCreationDto;
import com.test.factures.model.model.AdresseModel;
import com.test.factures.model.model.ClientCreationModel;
import com.test.factures.model.model.ClientModel;
import com.test.factures.service.data.AdresseDataService;
import com.test.factures.service.data.ClientDataService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {
    private final ClientDataService clientDataService;
    private final AdresseDataService adresseDataService;
    private final AdresseModificationDtoMapper adresseModificationDtoMapper;
    private final AdresseCreationDtoMapper adresseCreationDtoMapper;

    @Override
    public List<ClientModel> getAllClients() {
        return clientDataService.getAllClients();
    }

    @Override
    public ClientModel getClient(String codeClient) throws NotFoundException {
        return clientDataService.getClient(codeClient);
    }

    @Override
    @Transactional(rollbackOn = {RuntimeException.class, FactureException.class})
    public ClientModel creerClient(ClientCreationDto donneesClient) throws NotFoundException, FunctionalException {
        AdresseModel adresseCree = adresseDataService.creerAdresse(
                adresseCreationDtoMapper.toDestination(donneesClient.getAdresse())
        );
        return clientDataService.creerClient(
                new ClientCreationModel(donneesClient.getCode(), adresseCree.getId())
        );
    }

    @Override
    public AdresseModel modifierAdresseClient(String codeClient, AdresseModificationDto donneesAdresse) throws NotFoundException {
        ClientModel client = clientDataService.getClient(codeClient);
        return adresseDataService.modifierAdresse(client.getAdresse().getId(), adresseModificationDtoMapper.toDestination(donneesAdresse));
    }
}
