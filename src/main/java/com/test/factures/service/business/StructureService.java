package com.test.factures.service.business;

import com.test.factures.exception.NotFoundException;
import com.test.factures.model.dto.StructureCreationDto;
import com.test.factures.model.model.StructureModel;

import java.util.List;

public interface StructureService {
    List<StructureModel> getAllStructures();

    StructureModel getStructure(Integer idStructure) throws NotFoundException;

    StructureModel creerStructure(StructureCreationDto donneesStructure) throws NotFoundException;

}
