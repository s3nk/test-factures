package com.test.factures.service.business;

import com.test.factures.exception.FunctionalException;
import com.test.factures.exception.NotFoundException;
import com.test.factures.exception.TechnicalException;
import com.test.factures.model.dto.FactureCreationDto;
import com.test.factures.model.model.FactureModel;

import java.util.List;

public interface FactureService {
    List<FactureModel> getAllFactures(String codeClient);

    FactureModel getFacture(String referenceFacture) throws NotFoundException;

    FactureModel creerFacture(FactureCreationDto donneesFacture) throws TechnicalException, NotFoundException, FunctionalException;

}
