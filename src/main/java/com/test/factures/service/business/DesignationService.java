package com.test.factures.service.business;

import com.test.factures.exception.NotFoundException;
import com.test.factures.model.dto.DesignationCreationDto;
import com.test.factures.model.dto.DesignationModificationDto;
import com.test.factures.model.model.DesignationModel;

import java.util.List;

public interface DesignationService {
    List<DesignationModel> getAllDesignations();

    DesignationModel getDesignation(Integer idDesignation) throws NotFoundException;

    DesignationModel creerDesignation(DesignationCreationDto donneesDesignation) throws NotFoundException;

    DesignationModel modifierDesignation(Integer idDesignation, DesignationModificationDto donneesDesignation) throws NotFoundException;
}
