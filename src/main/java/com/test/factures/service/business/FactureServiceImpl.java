package com.test.factures.service.business;

import com.test.factures.exception.FunctionalException;
import com.test.factures.exception.NotFoundException;
import com.test.factures.exception.TechnicalException;
import com.test.factures.mapper.FactureCreationDtoMapper;
import com.test.factures.model.dto.FactureCreationDto;
import com.test.factures.model.model.FactureModel;
import com.test.factures.service.data.FactureDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FactureServiceImpl implements FactureService {
    private final FactureDataService factureDataService;
    private final FactureCreationDtoMapper factureCreationDtoMapper;

    @Override
    public List<FactureModel> getAllFactures(String codeClient) {
        return factureDataService.getAllFactures(codeClient);
    }

    @Override
    public FactureModel getFacture(String referenceFacture) throws NotFoundException {
        return factureDataService.getFacture(referenceFacture);
    }

    @Override
    public FactureModel creerFacture(FactureCreationDto donneesFacture) throws TechnicalException, NotFoundException, FunctionalException {
        return factureDataService.creerFacture(
                factureCreationDtoMapper.toDestination(donneesFacture)
        );
    }
}
