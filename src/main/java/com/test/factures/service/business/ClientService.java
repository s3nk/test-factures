package com.test.factures.service.business;

import com.test.factures.exception.FunctionalException;
import com.test.factures.exception.NotFoundException;
import com.test.factures.model.dto.AdresseModificationDto;
import com.test.factures.model.dto.ClientCreationDto;
import com.test.factures.model.model.AdresseModel;
import com.test.factures.model.model.ClientModel;

import java.util.List;

public interface ClientService {
    ClientModel getClient(String codeClient) throws NotFoundException;

    ClientModel creerClient(ClientCreationDto donneesClient) throws NotFoundException, FunctionalException;

    AdresseModel modifierAdresseClient(String codeClient, AdresseModificationDto donneesAdresse) throws NotFoundException;

    List<ClientModel> getAllClients();
}
