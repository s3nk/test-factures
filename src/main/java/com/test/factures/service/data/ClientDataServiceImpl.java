package com.test.factures.service.data;

import com.test.factures.data.entity.Client;
import com.test.factures.data.repository.AdresseRepository;
import com.test.factures.data.repository.ClientRepository;
import com.test.factures.exception.FunctionalException;
import com.test.factures.exception.NotFoundException;
import com.test.factures.mapper.ClientModelMapper;
import com.test.factures.model.model.ClientCreationModel;
import com.test.factures.model.model.ClientModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ClientDataServiceImpl implements ClientDataService {
    private final ClientRepository clientRepository;
    private final AdresseRepository adresseRepository;
    private final ClientModelMapper clientModelMapper;

    @Override
    public List<ClientModel> getAllClients() {
        return clientModelMapper.toDestination(
                clientRepository.findAll()
        );
    }

    @Override
    public ClientModel getClient(String codeClient) throws NotFoundException {
        return clientModelMapper.toDestination(
                clientRepository.findById(codeClient)
                        .orElseThrow(() -> new NotFoundException("Le client " + codeClient + " n'a pas été trouvé."))
        );
    }

    @Override
    public ClientModel creerClient(ClientCreationModel donneesClient) throws NotFoundException, FunctionalException {
        if (clientRepository.findById(donneesClient.getCode()).isPresent()) {
            throw new FunctionalException("Le client " + donneesClient.getCode() + " existe déjà.");
        }
        Client client = new Client();
        client.setCode(donneesClient.getCode());
        client.setAdresse(
                adresseRepository.findById(donneesClient.getIdAdresse())
                        .orElseThrow(() -> new NotFoundException("L'adresse " + donneesClient.getIdAdresse() + " n'a pas été trouvée."))
        );
        return clientModelMapper.toDestination(
                clientRepository.save(client)
        );
    }
}
