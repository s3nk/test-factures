package com.test.factures.service.data;

import com.test.factures.data.entity.Adresse;
import com.test.factures.data.repository.AdresseRepository;
import com.test.factures.exception.NotFoundException;
import com.test.factures.mapper.AdresseCreationModelMapper;
import com.test.factures.mapper.AdresseModelMapper;
import com.test.factures.model.model.AdresseCreationModel;
import com.test.factures.model.model.AdresseModel;
import com.test.factures.model.model.AdresseModificationModel;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AdresseDataServiceImpl implements AdresseDataService {
    private final AdresseRepository adresseRepository;
    private final AdresseModelMapper adresseModelMapper;
    private final AdresseCreationModelMapper adresseCreationModelMapper;

    @Override
    public AdresseModel getAdresse(Integer idAdresse) throws NotFoundException {
        return adresseModelMapper.toDestination(
                adresseRepository.findById(idAdresse)
                        .orElseThrow(() -> new NotFoundException("L'adresse " + idAdresse + " n'a pas été trouvée."))
        );
    }

    @Override
    public AdresseModel creerAdresse(AdresseCreationModel adresseCreationModel) {
        return adresseModelMapper.toDestination(
                adresseRepository.save(
                        adresseCreationModelMapper.toDestination(adresseCreationModel)
                )
        );
    }

    @Override
    public AdresseModel modifierAdresse(Integer idAdresse, AdresseModificationModel donneesAdresse) throws NotFoundException {
        Adresse adresse = adresseRepository.findById(idAdresse)
                .orElseThrow(() -> new NotFoundException("L'adresse " + idAdresse + " n'a pas été trouvée."));

        boolean modifiee = false;

        if (StringUtils.isNotBlank(donneesAdresse.getLigne()) && !donneesAdresse.getLigne().equals(adresse.getLigne())) {
            adresse.setLigne(donneesAdresse.getLigne());
            modifiee = true;
        }

        if (StringUtils.isNotBlank(donneesAdresse.getRaisonSociale()) && !donneesAdresse.getRaisonSociale().equals(adresse.getRaisonSociale())) {
            adresse.setRaisonSociale(donneesAdresse.getRaisonSociale());
            modifiee = true;
        }

        if (StringUtils.isNotBlank(donneesAdresse.getCodePostal()) && !donneesAdresse.getCodePostal().equals(adresse.getCodePostal())) {
            adresse.setCodePostal(donneesAdresse.getCodePostal());
            modifiee = true;
        }

        if (StringUtils.isNotBlank(donneesAdresse.getVille()) && !donneesAdresse.getVille().equals(adresse.getVille())) {
            adresse.setVille(donneesAdresse.getVille());
            modifiee = true;
        }

        if (modifiee) {
            adresse = adresseRepository.save(adresse);
        }

        return adresseModelMapper.toDestination(adresse);
    }
}
