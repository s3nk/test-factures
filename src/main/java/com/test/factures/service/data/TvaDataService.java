package com.test.factures.service.data;

import com.test.factures.exception.NotFoundException;
import com.test.factures.model.model.TvaCreationModel;
import com.test.factures.model.model.TvaModel;

import java.util.List;

public interface TvaDataService {
    List<TvaModel> getAllTva();

    TvaModel getTva(Integer idTva) throws NotFoundException;

    TvaModel creerTva(TvaCreationModel tvaCreationModel);
}
