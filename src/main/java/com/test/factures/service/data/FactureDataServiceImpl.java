package com.test.factures.service.data;

import com.test.factures.data.entity.*;
import com.test.factures.data.repository.*;
import com.test.factures.data.specification.FactureSpecifications;
import com.test.factures.exception.FactureException;
import com.test.factures.exception.FunctionalException;
import com.test.factures.exception.NotFoundException;
import com.test.factures.exception.TechnicalException;
import com.test.factures.mapper.FactureModelMapper;
import com.test.factures.model.dto.FactureCreationModel;
import com.test.factures.model.dto.FactureDesignationCreationModel;
import com.test.factures.model.enumeration.RefConditionsPaiementCode;
import com.test.factures.model.model.AdresseCreationModel;
import com.test.factures.model.model.AdresseModel;
import com.test.factures.model.model.FactureModel;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FactureDataServiceImpl implements FactureDataService {
    private final AdresseDataService adresseDataService;
    private final FactureRepository factureRepository;
    private final ClientRepository clientRepository;
    private final StructureRepository structureRepository;
    private final RefConditionsPaiementRepository conditionsPaiementRepository;
    private final FactureDesignationRepository factureDesignationRepository;
    private final DesignationRepository designationRepository;
    private final AdresseRepository adresseRepository;
    private final FactureModelMapper factureModelMapper;

    @Override
    public List<FactureModel> getAllFactures(String codeClient) {
        return factureModelMapper.toDestination(
                factureRepository.findAll(FactureSpecifications.searchBy(codeClient))
        );
    }

    @Override
    public FactureModel getFacture(String referenceFacture) throws NotFoundException {
        return factureModelMapper.toDestination(
                factureRepository.findById(referenceFacture)
                        .orElseThrow(() -> new NotFoundException("La facture " + referenceFacture + " n'a pas été trouvée."))
        );
    }

    @Override
    @Transactional(rollbackOn = {RuntimeException.class, FactureException.class})
    public FactureModel creerFacture(FactureCreationModel donneesFacture) throws FunctionalException, NotFoundException, TechnicalException {
        if (factureRepository.findById(donneesFacture.getReference()).isPresent()) {
            throw new FunctionalException("La facture " + donneesFacture.getReference() + " existe déjà.");
        }

        Client client = clientRepository.findById(donneesFacture.getCodeClient())
                .orElseThrow(() -> new NotFoundException("Le client " + donneesFacture.getCodeClient() + " n'a pas été trouvé."));

        Structure structure = structureRepository.findById(donneesFacture.getIdStructure())
                .orElseThrow(() -> new NotFoundException("La structure " + donneesFacture.getIdStructure() + " n'a pas été trouvée."));

        if (!RefConditionsPaiementCode.exists(donneesFacture.getCodeConditionsPaiement())) {
            throw new NotFoundException("La condition de paiement " + donneesFacture.getIdStructure() + " est inconnue.");
        }
        RefConditionsPaiement conditionsPaiement = conditionsPaiementRepository.findByCode(RefConditionsPaiementCode.fromDbValue(donneesFacture.getCodeConditionsPaiement()))
                .orElseThrow(() -> new NotFoundException("La condition de paiement " + donneesFacture.getIdStructure() + " n'a pas été trouvée."));

        AdresseModel copieAdresse = adresseDataService.creerAdresse(new AdresseCreationModel(
                client.getAdresse().getRaisonSociale(),
                client.getAdresse().getLigne(),
                client.getAdresse().getVille(),
                client.getAdresse().getCodePostal()
        ));

        Facture facture = new Facture();
        facture.setReference(donneesFacture.getReference());
        facture.setDateFacturation(LocalDate.now());
        facture.setDateEcheance(donneesFacture.getDateEcheance());
        facture.setClient(client);
        facture.setAdresse(
                adresseRepository.findById(copieAdresse.getId())
                        .orElseThrow(() -> new TechnicalException("L'adresse préalablement créée " + copieAdresse.getId() + " n'a pas été trouvée."))
        );
        facture.setStructure(structure);
        facture.setConditionsPaiement(conditionsPaiement);
        facture.setDesignations(new HashSet<>());
        facture = factureRepository.save(facture);

        for (FactureDesignationCreationModel designationCreationModel : donneesFacture.getDesignations()) {
            Designation designation = designationRepository.findById(designationCreationModel.getIdDesignation())
                    .orElseThrow(() -> new NotFoundException("La désignation " + designationCreationModel.getIdDesignation() + " n'a pas été trouvée."));

            // Les dates de début et de fin sont incluses, on doit ajouter 1 jour pour les controler
            List<DesignationTva> designationsTva = designation.getTvas().stream().filter(tva ->
                    tva.getDateDebut().isBefore(LocalDate.now().plusDays(1))
                            && (tva.getDateFin() == null || tva.getDateFin().isAfter(LocalDate.now().plusDays(1)))
            ).toList();
            if (designationsTva.size() > 1) {
                throw new FunctionalException("Plus d'une TVA active sur la désignation " + designationCreationModel.getIdDesignation() + ".");
            }
            if (designationsTva.isEmpty()) {
                throw new FunctionalException("Aucune TVA active sur la désignation " + designationCreationModel.getIdDesignation() + ".");
            }

            FactureDesignation factureDesignation = new FactureDesignation();
            factureDesignation.setFacture(facture);
            factureDesignation.setDesignation(designation);
            factureDesignation.setLibelleDesignation(designation.getLibelle());
            factureDesignation.setQuantite(designationCreationModel.getQuantite());
            factureDesignation.setPrixUnitaireHt(designation.getPrixUnitaireHt());
            Tva tva = designationsTva.get(0).getTva();
            factureDesignation.setTva(tva);
            factureDesignation.setMontantUnitaireTva(designation.getPrixUnitaireHt().multiply(tva.getTaux()).divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_DOWN));
            factureDesignation = factureDesignationRepository.save(factureDesignation);

            facture.getDesignations().add(factureDesignation);
        }

        return factureModelMapper.toDestination(
                factureRepository.save(facture)
        );
    }
}
