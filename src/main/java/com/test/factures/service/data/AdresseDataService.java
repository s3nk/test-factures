package com.test.factures.service.data;

import com.test.factures.exception.NotFoundException;
import com.test.factures.model.model.AdresseCreationModel;
import com.test.factures.model.model.AdresseModel;
import com.test.factures.model.model.AdresseModificationModel;

public interface AdresseDataService {
    AdresseModel getAdresse(Integer idAdresse) throws NotFoundException;

    AdresseModel creerAdresse(AdresseCreationModel adresseCreationModel);

    AdresseModel modifierAdresse(Integer idAdresse, AdresseModificationModel donneesAdresse) throws NotFoundException;
}
