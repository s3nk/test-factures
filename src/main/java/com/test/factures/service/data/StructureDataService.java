package com.test.factures.service.data;

import com.test.factures.exception.NotFoundException;
import com.test.factures.model.model.StructureCreationModel;
import com.test.factures.model.model.StructureModel;

import java.util.List;

public interface StructureDataService {
    List<StructureModel> getAllStructures();

    StructureModel getStructure(Integer idStructure) throws NotFoundException;

    StructureModel creerStructure(StructureCreationModel donneesStructure) throws NotFoundException;

}
