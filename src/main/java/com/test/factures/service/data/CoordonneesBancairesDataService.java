package com.test.factures.service.data;

import com.test.factures.exception.NotFoundException;
import com.test.factures.model.model.CoordonneesBancairesCreationModel;
import com.test.factures.model.model.CoordonneesBancairesModel;

public interface CoordonneesBancairesDataService {
    CoordonneesBancairesModel getCoordonneesBancaires(Integer idCoordonneesBancaires) throws NotFoundException;

    CoordonneesBancairesModel creerCoordonneesBancaires(CoordonneesBancairesCreationModel donneesCoordonneesBancaires);
}
