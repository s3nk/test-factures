package com.test.factures.service.data;

import com.test.factures.exception.NotFoundException;
import com.test.factures.model.model.DesignationCreationModel;
import com.test.factures.model.model.DesignationModel;
import com.test.factures.model.model.DesignationModificationModel;

import java.util.List;

public interface DesignationDataService {
    List<DesignationModel> getAllDesignations();

    DesignationModel getDesignation(Integer idDesignation) throws NotFoundException;

    DesignationModel creerDesignation(DesignationCreationModel donneesDesignation) throws NotFoundException;

    DesignationModel modifierDesignation(Integer idDesignation, DesignationModificationModel donneesDesignation) throws NotFoundException;
}
