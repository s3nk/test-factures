package com.test.factures.service.data;

import com.test.factures.data.repository.CoordonneesBancairesRepository;
import com.test.factures.exception.NotFoundException;
import com.test.factures.mapper.CoordonneesBancairesCreationModelMapper;
import com.test.factures.mapper.CoordonneesBancairesModelMapper;
import com.test.factures.model.model.CoordonneesBancairesCreationModel;
import com.test.factures.model.model.CoordonneesBancairesModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CoordonneesBancairesDataServiceImpl implements CoordonneesBancairesDataService {
    private final CoordonneesBancairesRepository coordonneesBancairesRepository;
    private final CoordonneesBancairesModelMapper coordonneesBancairesModelMapper;
    private final CoordonneesBancairesCreationModelMapper coordonneesBancairesCreationModelMapper;

    @Override
    public CoordonneesBancairesModel getCoordonneesBancaires(Integer idCoordonneesBancaires) throws NotFoundException {
        return coordonneesBancairesModelMapper.toDestination(
                coordonneesBancairesRepository.findById(idCoordonneesBancaires)
                        .orElseThrow(() -> new NotFoundException("Les coordonnées bancaires " + idCoordonneesBancaires + " n'ont pas été trouvées."))
        );
    }

    @Override
    public CoordonneesBancairesModel creerCoordonneesBancaires(CoordonneesBancairesCreationModel donneesCoordonneesBancaires) {
        return coordonneesBancairesModelMapper.toDestination(
                coordonneesBancairesRepository.save(
                        coordonneesBancairesCreationModelMapper.toDestination(donneesCoordonneesBancaires)
                )
        );
    }
}
