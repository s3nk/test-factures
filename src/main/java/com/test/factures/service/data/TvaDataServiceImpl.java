package com.test.factures.service.data;

import com.test.factures.data.repository.TvaRepository;
import com.test.factures.exception.NotFoundException;
import com.test.factures.mapper.TvaCreationModelMapper;
import com.test.factures.mapper.TvaModelMapper;
import com.test.factures.model.model.TvaCreationModel;
import com.test.factures.model.model.TvaModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TvaDataServiceImpl implements TvaDataService {
    private final TvaRepository tvaRepository;
    private final TvaModelMapper tvaModelMapper;
    private final TvaCreationModelMapper tvaCreationModelMapper;

    @Override
    public List<TvaModel> getAllTva() {
        return tvaModelMapper.toDestination(
                tvaRepository.findAll()
        );
    }

    @Override
    public TvaModel getTva(Integer idTva) throws NotFoundException {
        return tvaModelMapper.toDestination(
                tvaRepository.findById(idTva)
                        .orElseThrow(() -> new NotFoundException("La TVA " + idTva + " n'a pas été trouvée."))
        );
    }

    @Override
    public TvaModel creerTva(TvaCreationModel tvaCreationModel) {
        return tvaModelMapper.toDestination(
                tvaRepository.save(
                        tvaCreationModelMapper.toDestination(tvaCreationModel)
                )
        );
    }
}
