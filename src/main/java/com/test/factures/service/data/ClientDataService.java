package com.test.factures.service.data;

import com.test.factures.exception.FunctionalException;
import com.test.factures.exception.NotFoundException;
import com.test.factures.model.model.ClientCreationModel;
import com.test.factures.model.model.ClientModel;

import java.util.List;

public interface ClientDataService {
    ClientModel getClient(String codeClient) throws NotFoundException;

    ClientModel creerClient(ClientCreationModel donneesClient) throws NotFoundException, FunctionalException;

    List<ClientModel> getAllClients();
}
