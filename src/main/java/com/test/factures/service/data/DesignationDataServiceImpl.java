package com.test.factures.service.data;

import com.test.factures.data.entity.Designation;
import com.test.factures.data.entity.DesignationTva;
import com.test.factures.data.entity.Tva;
import com.test.factures.data.repository.DesignationRepository;
import com.test.factures.data.repository.DesignationTvaRepository;
import com.test.factures.data.repository.TvaRepository;
import com.test.factures.exception.FactureException;
import com.test.factures.exception.NotFoundException;
import com.test.factures.mapper.DesignationModelMapper;
import com.test.factures.model.model.DesignationCreationModel;
import com.test.factures.model.model.DesignationModel;
import com.test.factures.model.model.DesignationModificationModel;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DesignationDataServiceImpl implements DesignationDataService {
    private final DesignationRepository designationRepository;
    private final DesignationTvaRepository designationTvaRepository;
    private final TvaRepository tvaRepository;
    private final DesignationModelMapper designationModelMapper;

    @Override
    public List<DesignationModel> getAllDesignations() {
        return designationModelMapper.toDestination(
                designationRepository.findAll()
        );
    }

    @Override
    public DesignationModel getDesignation(Integer idDesignation) throws NotFoundException {
        return designationModelMapper.toDestination(
                designationRepository.findById(idDesignation)
                        .orElseThrow(() -> new NotFoundException("La designation " + idDesignation + " n'a pas été trouvée."))
        );
    }

    @Override
    @Transactional(rollbackOn = {RuntimeException.class, FactureException.class})
    public DesignationModel creerDesignation(DesignationCreationModel donneesDesignation) throws NotFoundException {
        Tva tva = tvaRepository.findById(donneesDesignation.getIdTva())
                .orElseThrow(() -> new NotFoundException("La TVA " + donneesDesignation.getIdTva() + " n'a pas été trouvée."));

        Designation designation = new Designation();
        designation.setLibelle(donneesDesignation.getLibelle());
        designation.setPrixUnitaireHt(donneesDesignation.getPrixUnitaireHt());
        designationRepository.save(designation);

        DesignationTva designationTva = new DesignationTva();
        designationTva.setDesignation(designation);
        designationTva.setTva(tva);
        designationTva.setDateDebut(LocalDate.now()); // Par défaut la TVA s'applique à l'article dès maintenant
        designationTvaRepository.save(designationTva);

        if (designation.getTvas() == null) {
            designation.setTvas(new HashSet<>());
        }
        designation.getTvas().add(designationTva);
        return designationModelMapper.toDestination(designationRepository.save(designation));
    }

    @Override
    @Transactional(rollbackOn = {RuntimeException.class, FactureException.class})
    public DesignationModel modifierDesignation(Integer idDesignation, DesignationModificationModel donneesDesignation) throws NotFoundException {
        Designation designation = designationRepository.findById(idDesignation)
                .orElseThrow(() -> new NotFoundException("La designation " + idDesignation + " n'a pas été trouvée."));

        boolean modifie = false;

        if (StringUtils.isNotBlank(donneesDesignation.getLibelle()) && !donneesDesignation.getLibelle().equals(designation.getLibelle())) {
            designation.setLibelle(donneesDesignation.getLibelle());
            modifie = true;
        }

        if (donneesDesignation.getPrixUnitaireHt() != null && !donneesDesignation.getPrixUnitaireHt().equals(designation.getPrixUnitaireHt())) {
            designation.setPrixUnitaireHt(donneesDesignation.getPrixUnitaireHt());
            modifie = true;
        }

        if (modifie) {
            designation = designationRepository.save(designation);
        }

        if (donneesDesignation.getNouvelleTva() != null) {
            if (designation.getTvas() == null) {
                designation.setTvas(new HashSet<>());
            }
            Optional<DesignationTva> oTva = designation.getTvas().stream().filter(tva -> tva.getTva().getId().equals(donneesDesignation.getNouvelleTva().getIdTva())).findFirst();
            if (oTva.isPresent()) { // Si on réutilise une TVA déjà présente
                DesignationTva designationTva = oTva.get();
                designationTva.setDateDebut(donneesDesignation.getNouvelleTva().getDateDebut());
                designationTva.setDateFin(null);
                designationTvaRepository.save(designationTva);
            } else {
                Tva tva = tvaRepository.findById(donneesDesignation.getNouvelleTva().getIdTva())
                        .orElseThrow(() -> new NotFoundException("La TVA " + donneesDesignation.getNouvelleTva().getIdTva() + " n'a pas été trouvée."));
                Optional<DesignationTva> oDesignationTvaASupprimer = designation.getTvas().stream().filter(tvaElem -> tvaElem.getDateFin() == null).findFirst();

                // On met une date de fin à la potentielle TVA actuellement sans date de fin
                if (oDesignationTvaASupprimer.isPresent()) {
                    DesignationTva designationTvaASupprimer = oDesignationTvaASupprimer.get();
                    designationTvaASupprimer.setDateFin(donneesDesignation.getNouvelleTva().getDateDebut().minusDays(1));
                    designationTvaRepository.save(designationTvaASupprimer);
                }

                // On crée la nouvelle TVA
                DesignationTva designationTva = new DesignationTva();
                designationTva.setDesignation(designation);
                designationTva.setTva(tva);
                designationTva.setDateDebut(donneesDesignation.getNouvelleTva().getDateDebut());
                designationTvaRepository.save(designationTva);

                designation.getTvas().add(designationTva);
            }
        }

        return designationModelMapper.toDestination(designationRepository.save(designation));
    }
}
