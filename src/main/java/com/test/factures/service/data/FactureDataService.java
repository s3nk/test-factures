package com.test.factures.service.data;

import com.test.factures.exception.FunctionalException;
import com.test.factures.exception.NotFoundException;
import com.test.factures.exception.TechnicalException;
import com.test.factures.model.dto.FactureCreationModel;
import com.test.factures.model.model.FactureModel;

import java.util.List;

public interface FactureDataService {

    List<FactureModel> getAllFactures(String codeClient);

    FactureModel getFacture(String referenceFacture) throws NotFoundException;

    FactureModel creerFacture(FactureCreationModel donneesFacture) throws FunctionalException, NotFoundException, TechnicalException;
}
