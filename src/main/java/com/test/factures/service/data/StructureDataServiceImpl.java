package com.test.factures.service.data;

import com.test.factures.data.entity.Structure;
import com.test.factures.data.repository.AdresseRepository;
import com.test.factures.data.repository.CoordonneesBancairesRepository;
import com.test.factures.data.repository.StructureRepository;
import com.test.factures.exception.NotFoundException;
import com.test.factures.mapper.StructureModelMapper;
import com.test.factures.model.model.StructureCreationModel;
import com.test.factures.model.model.StructureModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StructureDataServiceImpl implements StructureDataService {
    private final StructureRepository structureRepository;
    private final CoordonneesBancairesRepository coordonneesBancairesRepository;
    private final AdresseRepository adresseRepository;
    private final StructureModelMapper structureModelMapper;

    @Override
    public List<StructureModel> getAllStructures() {
        return structureModelMapper.toDestination(
                structureRepository.findAll()
        );
    }

    @Override
    public StructureModel getStructure(Integer idStructure) throws NotFoundException {
        return structureModelMapper.toDestination(
                structureRepository.findById(idStructure)
                        .orElseThrow(() -> new NotFoundException("La structure " + idStructure + " n'a pas été trouvée."))
        );
    }

    @Override
    public StructureModel creerStructure(StructureCreationModel donneesStructure) throws NotFoundException {
        Structure structure = new Structure();
        structure.setLibelle(donneesStructure.getLibelle());
        structure.setUrlLogo(donneesStructure.getUrlLogo());
        structure.setCoordonneesBancaires(
                coordonneesBancairesRepository.findById(donneesStructure.getIdCoordonneesBancaires())
                        .orElseThrow(() -> new NotFoundException("Les coordonnées bancaires " + donneesStructure.getIdCoordonneesBancaires() + " n'ont pas été trouvées."))
        );
        structure.setAdresse(
                adresseRepository.findById(donneesStructure.getIdAdresse())
                        .orElseThrow(() -> new NotFoundException("L'adresse " + donneesStructure.getIdAdresse() + " n'a pas été trouvée."))
        );
        return structureModelMapper.toDestination(
                structureRepository.save(structure)
        );
    }
}
