package com.test.factures.data.entity;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Data
public abstract class EntiteAuditee {

    @CreatedDate
    @Column(name = "date_creation")
    protected LocalDateTime dateCreation;

    @CreatedBy
    @Column(name = "user_creation")
    protected String userCreation;

    @LastModifiedDate
    @Column(name = "date_modification")
    protected LocalDateTime dateModification;

    @LastModifiedBy
    @Column(name = "user_modification")
    protected String userModification;

}