package com.test.factures.data.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "coordonnees_bancaires")
public class CoordonneesBancaires extends EntiteAuditee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "domiciliation", nullable = false, length = 50)
    private String domiciliation;

    @Column(name = "proprietaire", nullable = false, length = 200)
    private String proprietaire;

    @Column(name = "iban", nullable = false, length = 35)
    private String iban;

    @Column(name = "bic", nullable = false, length = 11)
    private String bic;
}
