package com.test.factures.data.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "structure")
public class Structure extends EntiteAuditee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "libelle", nullable = false, length = 200)
    private String libelle;

    @Column(name = "url_logo", nullable = false, length = 200)
    private String urlLogo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_adresse", nullable = false)
    private Adresse adresse;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_coordonnees_bancaires", nullable = false)
    private CoordonneesBancaires coordonneesBancaires;
}
