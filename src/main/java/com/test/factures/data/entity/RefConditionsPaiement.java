package com.test.factures.data.entity;

import com.test.factures.data.converter.RefConditionsPaiementCodeConverter;
import com.test.factures.model.enumeration.RefConditionsPaiementCode;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "ref_conditions_paiement")
public class RefConditionsPaiement extends EntiteAuditee {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer id;

    @Column(name = "code")
    @Convert(converter = RefConditionsPaiementCodeConverter.class)
    protected RefConditionsPaiementCode code;

    @Column(name = "libelle")
    protected String libelle;

}