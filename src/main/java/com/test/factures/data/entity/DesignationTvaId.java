package com.test.factures.data.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
public class DesignationTvaId implements Serializable {
    private Designation designation;
    private Tva tva;
}
