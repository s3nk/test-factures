package com.test.factures.data.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Entity
@IdClass(FactureDesignationId.class)
@Table(name = "facture_designation")
public class FactureDesignation extends EntiteAuditee {
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reference_facture", nullable = false)
    private Facture facture;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_designation", nullable = false)
    private Designation designation;

    @Column(name = "libelle_designation", nullable = false, length = 100)
    private String libelleDesignation;

    @Column(name = "prix_unitaire_ht", nullable = false)
    private BigDecimal prixUnitaireHt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tva", nullable = false)
    private Tva tva;

    @Column(name = "montant_unitaire_tva", nullable = false)
    private BigDecimal montantUnitaireTva;

    @Column(name = "quantite", nullable = false)
    private Double quantite;
}
