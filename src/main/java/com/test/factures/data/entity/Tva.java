package com.test.factures.data.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Table(name = "tva")
public class Tva extends EntiteAuditee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "taux", nullable = false)
    private BigDecimal taux;

    @Column(name = "libelle", nullable = false, length = 100)
    private String libelle;
}
