package com.test.factures.data.repository;

import com.test.factures.data.entity.CoordonneesBancaires;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoordonneesBancairesRepository extends JpaRepository<CoordonneesBancaires, Integer> {
}
