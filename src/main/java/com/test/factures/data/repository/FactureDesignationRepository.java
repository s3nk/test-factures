package com.test.factures.data.repository;

import com.test.factures.data.entity.FactureDesignation;
import com.test.factures.data.entity.FactureDesignationId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FactureDesignationRepository extends JpaRepository<FactureDesignation, FactureDesignationId> {
}
