package com.test.factures.data.repository;

import com.test.factures.data.entity.Facture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface FactureRepository extends JpaRepository<Facture, String>, JpaSpecificationExecutor<Facture> {
}
