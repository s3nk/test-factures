package com.test.factures.data.repository;

import com.test.factures.data.entity.RefConditionsPaiement;
import com.test.factures.model.enumeration.RefConditionsPaiementCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RefConditionsPaiementRepository extends JpaRepository<RefConditionsPaiement, Integer> {

    Optional<RefConditionsPaiement> findByCode(RefConditionsPaiementCode code);
}
