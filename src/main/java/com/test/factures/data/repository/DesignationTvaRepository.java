package com.test.factures.data.repository;

import com.test.factures.data.entity.DesignationTva;
import com.test.factures.data.entity.DesignationTvaId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DesignationTvaRepository extends JpaRepository<DesignationTva, DesignationTvaId> {
}
