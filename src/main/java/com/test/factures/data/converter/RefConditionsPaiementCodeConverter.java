package com.test.factures.data.converter;

import com.test.factures.model.enumeration.RefConditionsPaiementCode;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter
public class RefConditionsPaiementCodeConverter implements AttributeConverter<RefConditionsPaiementCode, String> {

    @Override
    public String convertToDatabaseColumn(RefConditionsPaiementCode refCode) {
        return refCode.getDbValue();
    }

    @Override
    public RefConditionsPaiementCode convertToEntityAttribute(String dbData) {
        return RefConditionsPaiementCode.fromDbValue(dbData);
    }
}

