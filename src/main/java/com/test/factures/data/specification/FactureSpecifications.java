package com.test.factures.data.specification;

import com.test.factures.data.entity.Facture;
import jakarta.persistence.criteria.Predicate;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class FactureSpecifications {

    public static Specification<Facture> searchBy(String codeClient) {
        return (factureRoot, query, cb) -> {
            query.distinct(true);

            List<Predicate> predicates = new ArrayList<>();

            if (StringUtils.isNotBlank(codeClient)) {
                predicates.add(cb.equal(factureRoot.get("client").get("code"), codeClient));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}