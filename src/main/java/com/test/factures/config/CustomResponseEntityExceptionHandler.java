package com.test.factures.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.factures.exception.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Pour modifier les réponses des appels avec des paramètres incorrects (les rendre plus lisibles)
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return ResponseEntity.badRequest().body(errors);
    }

    @ExceptionHandler(value = {BadRequestException.class})
    protected ResponseEntity<Object> handleBadRequestException(BadRequestException ex, WebRequest request) throws JsonProcessingException {
        return handleExceptionInternal(ex, new ObjectMapper().writeValueAsString(new FormattedException(ex)), getHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = {FunctionalException.class})
    protected ResponseEntity<Object> handleFunctionalException(FunctionalException ex, WebRequest request) throws JsonProcessingException {
        return handleExceptionInternal(ex, new ObjectMapper().writeValueAsString(new FormattedException(ex)), getHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(value = {NotFoundException.class})
    protected ResponseEntity<Object> handleNotFoundException(NotFoundException ex, WebRequest request) throws JsonProcessingException {
        return handleExceptionInternal(ex, new ObjectMapper().writeValueAsString(new FormattedException(ex)), getHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = {TechnicalException.class})
    protected ResponseEntity<Object> handleTechnicalException(TechnicalException ex, WebRequest request) throws JsonProcessingException {
        return handleExceptionInternal(ex, new ObjectMapper().writeValueAsString(new FormattedException(ex)), getHeaders(), HttpStatus.CONFLICT, request);
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        return headers;
    }
}
