package com.test.factures.config;

import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

public class CustomAuditorAware implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        // TODO : relier à un système d'authentification une fois disponible
        return Optional.of("factures-api");
    }

}
