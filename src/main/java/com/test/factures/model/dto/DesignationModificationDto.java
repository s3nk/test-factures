package com.test.factures.model.dto;

import jakarta.validation.Valid;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class DesignationModificationDto {
    private String libelle;

    private BigDecimal prixUnitaireHt;

    @Valid
    private DesignationTvaModificationDto nouvelleTva;
}
