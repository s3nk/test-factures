package com.test.factures.model.dto;

import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class AdresseModificationDto {
    @Size(max = 200)
    private String raisonSociale;

    @Size(max = 200)
    private String ligne;

    @Size(max = 100)
    private String ville;

    @Size(max = 10)
    private String codePostal;
}
