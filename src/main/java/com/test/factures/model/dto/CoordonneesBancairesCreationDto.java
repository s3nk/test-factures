package com.test.factures.model.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class CoordonneesBancairesCreationDto {
    @NotEmpty
    @Size(max = 50)
    private String domiciliation;

    @NotEmpty
    @Size(max = 200)
    private String proprietaire;

    @NotEmpty
    @Size(max = 35)
    private String iban;

    @NotEmpty
    @Size(max = 11)
    private String bic;
}
