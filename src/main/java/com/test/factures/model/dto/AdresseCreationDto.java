package com.test.factures.model.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class AdresseCreationDto {
    @NotEmpty
    @Size(max = 200)
    private String raisonSociale;

    @NotEmpty
    @Size(max = 200)
    private String ligne;

    @NotEmpty
    @Size(max = 100)
    private String ville;

    @NotEmpty
    @Size(max = 10)
    private String codePostal;
}
