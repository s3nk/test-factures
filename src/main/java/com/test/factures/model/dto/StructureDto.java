package com.test.factures.model.dto;

import lombok.Data;

@Data
public class StructureDto {
    private Integer id;

    private String libelle;

    private String urlLogo;

    private AdresseDto adresse;

    private CoordonneesBancairesDto coordonneesBancaires;
}
