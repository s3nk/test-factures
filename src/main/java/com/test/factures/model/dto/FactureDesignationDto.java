package com.test.factures.model.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class FactureDesignationDto {

    private DesignationDto designation;

    private String libelleDesignation;

    private BigDecimal prixUnitaireHt;

    private TvaDto tva;

    private BigDecimal montantUnitaireTva;

    private Double quantite;
}
