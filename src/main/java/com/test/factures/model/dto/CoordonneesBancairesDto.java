package com.test.factures.model.dto;

import lombok.Data;

@Data
public class CoordonneesBancairesDto {

    private Integer id;

    private String domiciliation;

    private String proprietaire;

    private String iban;

    private String bic;
}
