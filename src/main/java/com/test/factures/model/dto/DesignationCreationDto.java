package com.test.factures.model.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class DesignationCreationDto {
    @NotEmpty
    @Size(max = 100)
    private String libelle;

    @NotNull
    private BigDecimal prixUnitaireHt;

    @NotNull
    private Integer idTva;
}
