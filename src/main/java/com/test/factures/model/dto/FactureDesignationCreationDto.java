package com.test.factures.model.dto;

import lombok.Data;

@Data
public class FactureDesignationCreationDto {
    private Integer idDesignation;

    private Double quantite;
}
