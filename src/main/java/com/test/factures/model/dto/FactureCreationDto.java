package com.test.factures.model.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class FactureCreationDto {
    @NotEmpty
    @Size(max = 10)
    private String reference;

    @NotEmpty
    @Size(max = 15)
    private String codeClient;

    @NotNull
    private Integer idStructure;

    @NotEmpty
    @Size(max = 50)
    private String codeConditionsPaiement;

    @NotNull
    private LocalDate dateEcheance;

    @NotEmpty
    private List<FactureDesignationCreationDto> designations;
}
