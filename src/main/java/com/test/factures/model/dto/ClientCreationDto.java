package com.test.factures.model.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class ClientCreationDto {

    @NotEmpty
    @Size(max = 15)
    private String code;

    @NotNull
    @Valid
    private AdresseCreationDto adresse;
}
