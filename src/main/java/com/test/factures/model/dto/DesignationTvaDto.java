package com.test.factures.model.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class DesignationTvaDto {
    private TvaDto tva;

    private LocalDate dateDebut;

    private LocalDate dateFin;
}
