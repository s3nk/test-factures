package com.test.factures.model.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class StructureCreationDto {

    @NotEmpty
    @Size(max = 200)
    private String libelle;

    @NotEmpty
    @Size(max = 200)
    private String urlLogo;

    @NotNull
    @Valid
    private AdresseCreationDto adresse;

    @NotNull
    @Valid
    private CoordonneesBancairesCreationDto coordonneesBancaires;
}
