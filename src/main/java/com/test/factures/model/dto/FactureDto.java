package com.test.factures.model.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.Set;

@Data
public class FactureDto {

    private String reference;

    private LocalDate dateFacturation;

    private LocalDate dateEcheance;

    private AdresseDto adresse;

    private ClientDto client;

    private StructureDto structure;

    private String codeConditionsPaiement; // Peut contenir les valeurs de l'enumération RefConditionsPaiementCode

    private Set<FactureDesignationDto> designations;
}
