package com.test.factures.model.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TvaDto {

    private Integer id;

    private BigDecimal taux;

    private String libelle;
}
