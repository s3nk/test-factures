package com.test.factures.model.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class DesignationDto {
    private Integer id;

    private String libelle;

    private BigDecimal prixUnitaireHt;

    private List<DesignationTvaDto> tvas;
}
