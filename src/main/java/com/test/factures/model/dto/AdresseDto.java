package com.test.factures.model.dto;

import lombok.Data;

@Data
public class AdresseDto {

    private Integer id;

    private String raisonSociale;

    private String ligne;

    private String ville;

    private String codePostal;
}
