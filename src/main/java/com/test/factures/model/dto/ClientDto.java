package com.test.factures.model.dto;

import lombok.Data;

@Data
public class ClientDto {
    private String code;

    private AdresseDto adresse;
}
