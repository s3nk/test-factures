package com.test.factures.model.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.time.LocalDate;

@Data
public class DesignationTvaModificationDto {
    @NotNull
    private Integer idTva;

    @NotNull
    private LocalDate dateDebut;
}
