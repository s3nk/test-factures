package com.test.factures.model.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class FactureCreationModel {
    private String reference;

    private String codeClient;

    private Integer idStructure;

    private String codeConditionsPaiement;

    private LocalDate dateEcheance;

    private List<FactureDesignationCreationModel> designations;
}
