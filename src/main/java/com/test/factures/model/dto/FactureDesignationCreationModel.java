package com.test.factures.model.dto;

import lombok.Data;

@Data
public class FactureDesignationCreationModel {
    private Integer idDesignation;

    private Double quantite;
}
