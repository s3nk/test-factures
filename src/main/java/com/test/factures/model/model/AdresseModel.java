package com.test.factures.model.model;

import lombok.Data;

@Data
public class AdresseModel {

    private Integer id;

    private String raisonSociale;

    private String ligne;

    private String ville;

    private String codePostal;
}
