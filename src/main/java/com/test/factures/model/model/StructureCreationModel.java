package com.test.factures.model.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StructureCreationModel {

    private String libelle;

    private String urlLogo;

    private Integer idAdresse;

    private Integer idCoordonneesBancaires;
}
