package com.test.factures.model.model;

import lombok.Data;

@Data
public class StructureModel {
    private Integer id;

    private String libelle;

    private String urlLogo;

    private AdresseModel adresse;

    private CoordonneesBancairesModel coordonneesBancaires;
}
