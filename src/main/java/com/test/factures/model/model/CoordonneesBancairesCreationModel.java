package com.test.factures.model.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CoordonneesBancairesCreationModel {

    private String domiciliation;

    private String proprietaire;

    private String iban;

    private String bic;
}
