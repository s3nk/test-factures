package com.test.factures.model.model;

import lombok.Data;

import java.time.LocalDate;

@Data
public class DesignationTvaModel {
    private TvaModel tva;

    private LocalDate dateDebut;

    private LocalDate dateFin;
}
