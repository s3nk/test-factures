package com.test.factures.model.model;

import com.test.factures.model.enumeration.RefConditionsPaiementCode;
import lombok.Data;

import java.time.LocalDate;
import java.util.Set;

@Data
public class FactureModel {

    private String reference;

    private LocalDate dateFacturation;

    private LocalDate dateEcheance;

    private AdresseModel adresse;

    private ClientModel client;

    private StructureModel structure;

    private RefConditionsPaiementCode codeConditionsPaiement;

    private Set<FactureDesignationModel> designations;
}
