package com.test.factures.model.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class FactureDesignationModel {
    private DesignationModel designation;

    private String libelleDesignation;

    private BigDecimal prixUnitaireHt;

    private TvaModel tva;

    private BigDecimal montantUnitaireTva;

    private Double quantite;
}
