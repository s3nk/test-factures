package com.test.factures.model.model;

import lombok.Data;

@Data
public class CoordonneesBancairesModel {

    private Integer id;

    private String domiciliation;

    private String proprietaire;

    private String iban;

    private String bic;
}
