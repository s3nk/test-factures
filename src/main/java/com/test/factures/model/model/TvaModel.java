package com.test.factures.model.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TvaModel {

    private Integer id;

    private BigDecimal taux;

    private String libelle;
}
