package com.test.factures.model.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class DesignationCreationModel {
    private String libelle;
    private BigDecimal prixUnitaireHt;
    private Integer idTva;
}
