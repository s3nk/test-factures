package com.test.factures.model.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AdresseCreationModel {

    private String raisonSociale;

    private String ligne;

    private String ville;

    private String codePostal;
}
