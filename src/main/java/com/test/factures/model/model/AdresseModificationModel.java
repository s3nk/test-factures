package com.test.factures.model.model;

import lombok.Data;

@Data
public class AdresseModificationModel {
    
    private String raisonSociale;

    private String ligne;

    private String ville;

    private String codePostal;
}
