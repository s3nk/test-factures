package com.test.factures.model.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClientCreationModel {
    private String code;

    private Integer idAdresse;
}
