package com.test.factures.model.model;

import lombok.Data;

@Data
public class ClientModel {
    private String code;

    private AdresseModel adresse;
}
