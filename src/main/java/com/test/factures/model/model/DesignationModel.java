package com.test.factures.model.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class DesignationModel {
    private Integer id;

    private String libelle;

    private BigDecimal prixUnitaireHt;

    private List<DesignationTvaModel> tvas;
}
