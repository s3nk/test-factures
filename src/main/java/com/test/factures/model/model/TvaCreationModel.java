package com.test.factures.model.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TvaCreationModel {

    private BigDecimal taux;

    private String libelle;
}
