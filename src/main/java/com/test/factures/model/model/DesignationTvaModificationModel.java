package com.test.factures.model.model;

import lombok.Data;

import java.time.LocalDate;

@Data
public class DesignationTvaModificationModel {
    private Integer idTva;

    private LocalDate dateDebut;
}
