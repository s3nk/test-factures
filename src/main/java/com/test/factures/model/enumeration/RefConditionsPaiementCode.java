package com.test.factures.model.enumeration;

import java.util.HashMap;
import java.util.Map;

public enum RefConditionsPaiementCode {

    REGLEMENT_LIVRAISON("REGLEMENT_LIVRAISON");

    private static final Map<String, RefConditionsPaiementCode> dbValues = new HashMap<>();

    static {
        for (RefConditionsPaiementCode value : values()) {
            dbValues.put(value.dbValue, value);
        }
    }

    private String dbValue;

    RefConditionsPaiementCode(String dbValue) {
        this.dbValue = dbValue;
    }

    public static RefConditionsPaiementCode fromDbValue(String dbValue) {
        return dbValues.get(dbValue);
    }

    public static boolean exists(String dbValue) {
        return dbValues.containsKey(dbValue);
    }

    public String getDbValue() {
        return dbValue;
    }

}
