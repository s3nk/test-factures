package com.test.factures.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class FunctionalException extends FactureException {
    public FunctionalException(String message) {
        super(message);
    }
}
