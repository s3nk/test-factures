package com.test.factures.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class NotFoundException extends FactureException {
    public NotFoundException(String message) {
        super(message);
    }
}
