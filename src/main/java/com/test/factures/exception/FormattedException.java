package com.test.factures.exception;

import lombok.Data;

@Data
public class FormattedException {
    private String message;

    public FormattedException(FactureException e) {
        this.message = e.getMessage();
    }
}
