package com.test.factures.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class TechnicalException extends FactureException {
    public TechnicalException(String message) {
        super(message);
    }
}
