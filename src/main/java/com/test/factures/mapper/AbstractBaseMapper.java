package com.test.factures.mapper;

public abstract class AbstractBaseMapper<Source, Destination> implements BaseMapper<Source, Destination> {

    abstract public Destination toDestination(Source source);

    abstract public Source toSource(Destination destination);


}
