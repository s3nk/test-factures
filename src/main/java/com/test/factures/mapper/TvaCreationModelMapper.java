package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.data.entity.Tva;
import com.test.factures.model.model.TvaCreationModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapstructConfig.class, uses = {})
public interface TvaCreationModelMapper extends SourceToDestinationMapper<TvaCreationModel, Tva> {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "dateCreation", ignore = true)
    @Mapping(target = "userCreation", ignore = true)
    @Mapping(target = "dateModification", ignore = true)
    @Mapping(target = "userModification", ignore = true)
    Tva toDestination(TvaCreationModel obj);
}
