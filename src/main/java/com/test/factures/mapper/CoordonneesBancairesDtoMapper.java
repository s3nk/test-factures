package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.dto.CoordonneesBancairesDto;
import com.test.factures.model.model.CoordonneesBancairesModel;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {})
public interface CoordonneesBancairesDtoMapper extends BaseMapper<CoordonneesBancairesModel, CoordonneesBancairesDto> {
    CoordonneesBancairesModel toSource(CoordonneesBancairesDto obj);

    CoordonneesBancairesDto toDestination(CoordonneesBancairesModel obj);
}
