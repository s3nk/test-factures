package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.dto.FactureCreationDto;
import com.test.factures.model.dto.FactureCreationModel;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {FactureDesignationCreationDtoMapper.class})
public interface FactureCreationDtoMapper extends SourceToDestinationMapper<FactureCreationDto, FactureCreationModel> {
    FactureCreationModel toDestination(FactureCreationDto obj);
}
