package com.test.factures.mapper;


import org.springframework.data.domain.Page;

import java.util.List;
import java.util.stream.Collectors;

public interface DestinationToSourceMapper<Destination, Source> {
    Source toSource(Destination obj);

    default List<Source> toSource(List<Destination> list) {
        if (list == null) {
            return null;
        }
        return list.stream().map(d -> toSource((d))).collect(Collectors.toList());
    }

    default Page<Source> toSource(Page<Destination> page) {
        return page.map(e -> toSource(e));
    }
}
