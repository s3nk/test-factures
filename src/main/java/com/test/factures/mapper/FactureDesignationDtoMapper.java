package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.dto.FactureDesignationDto;
import com.test.factures.model.model.FactureDesignationModel;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {DesignationDtoMapper.class, TvaDtoMapper.class})
public interface FactureDesignationDtoMapper extends BaseMapper<FactureDesignationModel, FactureDesignationDto> {
    FactureDesignationModel toSource(FactureDesignationDto obj);

    FactureDesignationDto toDestination(FactureDesignationModel obj);
}
