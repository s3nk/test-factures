package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.dto.ClientDto;
import com.test.factures.model.model.ClientModel;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {AdresseDtoMapper.class})
public interface ClientDtoMapper extends BaseMapper<ClientModel, ClientDto> {
    ClientModel toSource(ClientDto obj);

    ClientDto toDestination(ClientModel obj);
}
