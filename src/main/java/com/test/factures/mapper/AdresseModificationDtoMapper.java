package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.dto.AdresseModificationDto;
import com.test.factures.model.model.AdresseModificationModel;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {})
public interface AdresseModificationDtoMapper extends SourceToDestinationMapper<AdresseModificationDto, AdresseModificationModel> {
    AdresseModificationModel toDestination(AdresseModificationDto obj);
}
