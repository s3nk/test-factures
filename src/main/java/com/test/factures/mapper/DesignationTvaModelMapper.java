package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.data.entity.DesignationTva;
import com.test.factures.model.model.DesignationTvaModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapstructConfig.class, uses = {TvaModelMapper.class})
public interface DesignationTvaModelMapper extends BaseMapper<DesignationTva, DesignationTvaModel> {
    @Mapping(target = "designation", ignore = true)
    @Mapping(target = "dateCreation", ignore = true)
    @Mapping(target = "userCreation", ignore = true)
    @Mapping(target = "dateModification", ignore = true)
    @Mapping(target = "userModification", ignore = true)
    DesignationTva toSource(DesignationTvaModel obj);

    DesignationTvaModel toDestination(DesignationTva obj);
}
