package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.data.entity.Client;
import com.test.factures.model.model.ClientModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapstructConfig.class, uses = {AdresseModelMapper.class})
public interface ClientModelMapper extends BaseMapper<Client, ClientModel> {
    @Mapping(target = "dateCreation", ignore = true)
    @Mapping(target = "userCreation", ignore = true)
    @Mapping(target = "dateModification", ignore = true)
    @Mapping(target = "userModification", ignore = true)
    Client toSource(ClientModel obj);

    ClientModel toDestination(Client obj);
}
