package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.dto.AdresseCreationDto;
import com.test.factures.model.model.AdresseCreationModel;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {})
public interface AdresseCreationDtoMapper extends SourceToDestinationMapper<AdresseCreationDto, AdresseCreationModel> {
    AdresseCreationModel toDestination(AdresseCreationDto obj);
}
