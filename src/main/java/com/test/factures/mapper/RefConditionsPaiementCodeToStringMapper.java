package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.enumeration.RefConditionsPaiementCode;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class)
public abstract class RefConditionsPaiementCodeToStringMapper extends AbstractBaseMapper<RefConditionsPaiementCode, String> {

    @Override
    public String toDestination(RefConditionsPaiementCode refConditionsPaiementCode) {
        return refConditionsPaiementCode.getDbValue();
    }

    @Override
    public RefConditionsPaiementCode toSource(String code) {
        return RefConditionsPaiementCode.exists(code) ? RefConditionsPaiementCode.fromDbValue(code) : null;
    }
}
