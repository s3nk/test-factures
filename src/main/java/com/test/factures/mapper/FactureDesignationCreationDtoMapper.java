package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.dto.FactureDesignationCreationDto;
import com.test.factures.model.dto.FactureDesignationCreationModel;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {})
public interface FactureDesignationCreationDtoMapper extends SourceToDestinationMapper<FactureDesignationCreationDto, FactureDesignationCreationModel> {
    FactureDesignationCreationModel toDestination(FactureDesignationCreationDto obj);
}
