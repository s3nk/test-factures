package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.data.entity.FactureDesignation;
import com.test.factures.model.model.FactureDesignationModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapstructConfig.class, uses = {DesignationModelMapper.class, TvaModelMapper.class})
public interface FactureDesignationModelMapper extends BaseMapper<FactureDesignation, FactureDesignationModel> {
    @Mapping(target = "facture", ignore = true)
    @Mapping(target = "dateCreation", ignore = true)
    @Mapping(target = "userCreation", ignore = true)
    @Mapping(target = "dateModification", ignore = true)
    @Mapping(target = "userModification", ignore = true)
    FactureDesignation toSource(FactureDesignationModel obj);

    FactureDesignationModel toDestination(FactureDesignation obj);
}
