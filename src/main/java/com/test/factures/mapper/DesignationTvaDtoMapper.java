package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.dto.DesignationTvaDto;
import com.test.factures.model.model.DesignationTvaModel;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {TvaDtoMapper.class})
public interface DesignationTvaDtoMapper extends BaseMapper<DesignationTvaModel, DesignationTvaDto> {
    DesignationTvaModel toSource(DesignationTvaDto obj);

    DesignationTvaDto toDestination(DesignationTvaModel obj);
}
