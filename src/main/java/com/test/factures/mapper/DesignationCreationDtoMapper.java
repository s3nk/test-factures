package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.dto.DesignationCreationDto;
import com.test.factures.model.model.DesignationCreationModel;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {})
public interface DesignationCreationDtoMapper extends SourceToDestinationMapper<DesignationCreationDto, DesignationCreationModel> {
    DesignationCreationModel toDestination(DesignationCreationDto obj);
}
