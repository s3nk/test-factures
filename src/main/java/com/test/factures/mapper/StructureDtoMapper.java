package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.dto.StructureDto;
import com.test.factures.model.model.StructureModel;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {AdresseDtoMapper.class, CoordonneesBancairesDtoMapper.class})
public interface StructureDtoMapper extends BaseMapper<StructureModel, StructureDto> {
    StructureModel toSource(StructureDto obj);

    StructureDto toDestination(StructureModel obj);
}
