package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.dto.DesignationTvaModificationDto;
import com.test.factures.model.model.DesignationTvaModificationModel;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {})
public interface DesignationTvaModificationDtoMapper extends SourceToDestinationMapper<DesignationTvaModificationDto, DesignationTvaModificationModel> {
    DesignationTvaModificationModel toDestination(DesignationTvaModificationDto obj);
}
