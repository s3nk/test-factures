package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.data.entity.CoordonneesBancaires;
import com.test.factures.model.model.CoordonneesBancairesModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapstructConfig.class, uses = {})
public interface CoordonneesBancairesModelMapper extends BaseMapper<CoordonneesBancaires, CoordonneesBancairesModel> {
    @Mapping(target = "dateCreation", ignore = true)
    @Mapping(target = "userCreation", ignore = true)
    @Mapping(target = "dateModification", ignore = true)
    @Mapping(target = "userModification", ignore = true)
    CoordonneesBancaires toSource(CoordonneesBancairesModel obj);

    CoordonneesBancairesModel toDestination(CoordonneesBancaires obj);
}
