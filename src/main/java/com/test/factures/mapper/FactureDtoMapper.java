package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.dto.FactureDto;
import com.test.factures.model.model.FactureModel;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {AdresseDtoMapper.class, ClientDtoMapper.class, StructureDtoMapper.class, FactureDesignationDtoMapper.class, RefConditionsPaiementCodeToStringMapper.class})
public interface FactureDtoMapper extends BaseMapper<FactureModel, FactureDto> {
    FactureModel toSource(FactureDto obj);

    FactureDto toDestination(FactureModel obj);
}
