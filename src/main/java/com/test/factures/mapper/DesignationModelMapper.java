package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.data.entity.Designation;
import com.test.factures.model.model.DesignationModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapstructConfig.class, uses = {DesignationTvaModelMapper.class})
public interface DesignationModelMapper extends BaseMapper<Designation, DesignationModel> {
    @Mapping(target = "dateCreation", ignore = true)
    @Mapping(target = "userCreation", ignore = true)
    @Mapping(target = "dateModification", ignore = true)
    @Mapping(target = "userModification", ignore = true)
    Designation toSource(DesignationModel obj);

    DesignationModel toDestination(Designation obj);
}
