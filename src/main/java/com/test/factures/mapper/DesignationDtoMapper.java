package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.dto.DesignationDto;
import com.test.factures.model.model.DesignationModel;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {AdresseDtoMapper.class, DesignationTvaDtoMapper.class})
public interface DesignationDtoMapper extends BaseMapper<DesignationModel, DesignationDto> {
    DesignationModel toSource(DesignationDto obj);

    DesignationDto toDestination(DesignationModel obj);
}
