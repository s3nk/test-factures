package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.data.entity.Structure;
import com.test.factures.model.model.StructureModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapstructConfig.class, uses = {AdresseModelMapper.class, CoordonneesBancairesModelMapper.class})
public interface StructureModelMapper extends BaseMapper<Structure, StructureModel> {
    @Mapping(target = "dateCreation", ignore = true)
    @Mapping(target = "userCreation", ignore = true)
    @Mapping(target = "dateModification", ignore = true)
    @Mapping(target = "userModification", ignore = true)
    Structure toSource(StructureModel obj);

    StructureModel toDestination(Structure obj);
}
