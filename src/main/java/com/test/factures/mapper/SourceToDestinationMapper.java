package com.test.factures.mapper;

import org.springframework.data.domain.Page;

import java.util.List;
import java.util.stream.Collectors;

public interface SourceToDestinationMapper<Source, Destination> {
    Destination toDestination(Source obj);

    default List<Destination> toDestination(List<Source> list) {
        if (list == null) {
            return null;
        }
        return list.stream().map(d -> toDestination((d))).collect(Collectors.toList());
    }

    default Page<Destination> toDestination(Page<Source> page) {
        return page.map(e -> toDestination(e));
    }
}
