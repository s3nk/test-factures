package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.dto.TvaCreationDto;
import com.test.factures.model.model.TvaCreationModel;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {})
public interface TvaCreationDtoMapper extends SourceToDestinationMapper<TvaCreationDto, TvaCreationModel> {
    TvaCreationModel toDestination(TvaCreationDto obj);
}
