package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.dto.AdresseDto;
import com.test.factures.model.model.AdresseModel;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {})
public interface AdresseDtoMapper extends BaseMapper<AdresseModel, AdresseDto> {
    AdresseModel toSource(AdresseDto obj);

    AdresseDto toDestination(AdresseModel obj);
}
