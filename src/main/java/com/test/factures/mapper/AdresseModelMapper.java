package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.data.entity.Adresse;
import com.test.factures.model.model.AdresseModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapstructConfig.class, uses = {})
public interface AdresseModelMapper extends BaseMapper<Adresse, AdresseModel> {
    @Mapping(target = "dateCreation", ignore = true)
    @Mapping(target = "userCreation", ignore = true)
    @Mapping(target = "dateModification", ignore = true)
    @Mapping(target = "userModification", ignore = true)
    Adresse toSource(AdresseModel obj);

    AdresseModel toDestination(Adresse obj);
}
