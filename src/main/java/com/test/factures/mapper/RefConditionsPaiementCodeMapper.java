package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.data.entity.RefConditionsPaiement;
import com.test.factures.data.repository.RefConditionsPaiementRepository;
import com.test.factures.model.enumeration.RefConditionsPaiementCode;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = MapstructConfig.class)
public abstract class RefConditionsPaiementCodeMapper extends AbstractBaseMapper<RefConditionsPaiement, RefConditionsPaiementCode> {

    @Autowired
    private RefConditionsPaiementRepository refConditionsPaiementRepository;

    public RefConditionsPaiement toSource(RefConditionsPaiementCode refConditionsPaiementCode) {
        return refConditionsPaiementRepository.findByCode(refConditionsPaiementCode).orElseGet(null);
    }

    public RefConditionsPaiementCode toDestination(RefConditionsPaiement ref) {
        return ref.getCode();
    }

}
