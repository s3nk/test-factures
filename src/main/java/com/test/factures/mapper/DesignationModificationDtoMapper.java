package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.dto.DesignationModificationDto;
import com.test.factures.model.model.DesignationModificationModel;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {DesignationTvaModificationDtoMapper.class})
public interface DesignationModificationDtoMapper extends SourceToDestinationMapper<DesignationModificationDto, DesignationModificationModel> {
    DesignationModificationModel toDestination(DesignationModificationDto obj);
}
