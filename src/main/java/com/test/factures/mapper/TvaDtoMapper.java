package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.dto.TvaDto;
import com.test.factures.model.model.TvaModel;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {})
public interface TvaDtoMapper extends BaseMapper<TvaModel, TvaDto> {
    TvaModel toSource(TvaDto obj);

    TvaDto toDestination(TvaModel obj);
}
