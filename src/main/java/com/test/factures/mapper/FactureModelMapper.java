package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.data.entity.Facture;
import com.test.factures.model.model.FactureModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapstructConfig.class, uses = {AdresseModelMapper.class, ClientModelMapper.class,
        StructureModelMapper.class, FactureDesignationModelMapper.class, RefConditionsPaiementCodeMapper.class})
public interface FactureModelMapper extends BaseMapper<Facture, FactureModel> {
    @Mapping(source = "codeConditionsPaiement", target = "conditionsPaiement")
    @Mapping(target = "dateCreation", ignore = true)
    @Mapping(target = "userCreation", ignore = true)
    @Mapping(target = "dateModification", ignore = true)
    @Mapping(target = "userModification", ignore = true)
    Facture toSource(FactureModel obj);

    @Mapping(source = "conditionsPaiement", target = "codeConditionsPaiement")
    FactureModel toDestination(Facture obj);
}
