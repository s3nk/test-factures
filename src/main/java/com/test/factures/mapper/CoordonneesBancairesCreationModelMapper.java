package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.data.entity.CoordonneesBancaires;
import com.test.factures.model.model.CoordonneesBancairesCreationModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapstructConfig.class, uses = {})
public interface CoordonneesBancairesCreationModelMapper extends SourceToDestinationMapper<CoordonneesBancairesCreationModel, CoordonneesBancaires> {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "dateCreation", ignore = true)
    @Mapping(target = "userCreation", ignore = true)
    @Mapping(target = "dateModification", ignore = true)
    @Mapping(target = "userModification", ignore = true)
    CoordonneesBancaires toDestination(CoordonneesBancairesCreationModel obj);
}
