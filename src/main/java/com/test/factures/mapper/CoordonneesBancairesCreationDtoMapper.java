package com.test.factures.mapper;

import com.test.factures.config.MapstructConfig;
import com.test.factures.model.dto.CoordonneesBancairesCreationDto;
import com.test.factures.model.model.CoordonneesBancairesCreationModel;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {})
public interface CoordonneesBancairesCreationDtoMapper extends SourceToDestinationMapper<CoordonneesBancairesCreationDto, CoordonneesBancairesCreationModel> {
    CoordonneesBancairesCreationModel toDestination(CoordonneesBancairesCreationDto obj);
}
