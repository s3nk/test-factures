package com.test.factures.mapper;

public interface BaseMapper<Entity, Dto> extends SourceToDestinationMapper<Entity, Dto>, DestinationToSourceMapper<Dto, Entity> {

}
