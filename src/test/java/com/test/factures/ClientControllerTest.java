package com.test.factures;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.factures.data.repository.ClientRepository;
import com.test.factures.exception.FunctionalException;
import com.test.factures.exception.NotFoundException;
import com.test.factures.model.dto.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@AutoConfigureMockMvc
public class ClientControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ClientRepository clientRepository;

    /**
     * Methode qui efface
     */
    @AfterEach
    public void nettoyageClients() {
        clientRepository.deleteAll();
    }

    @Test
    public void getAllClients_RetourneListeVide() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/clients"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    @Test
    public void getAllClients_ValeurSiExistant() throws Exception {
        // On vérifie son absence en entrée
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/clients"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("[]"));

        ClientCreationDto clientCreationDto = new ClientCreationDto();
        clientCreationDto.setCode("CLIENT_1");
        AdresseCreationDto adresseCreationDto = new AdresseCreationDto();
        adresseCreationDto.setRaisonSociale("Client 1");
        adresseCreationDto.setCodePostal("14000");
        adresseCreationDto.setVille("Caen");
        adresseCreationDto.setLigne("1 rue de l'église");
        clientCreationDto.setAdresse(adresseCreationDto);
        this.mockMvc.perform(
                        MockMvcRequestBuilders.post("/api/v1/clients")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(clientCreationDto))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());

        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/clients"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        List<ClientDto> clientsRecus = objectMapper.readValue(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8), new TypeReference<List<ClientDto>>() {
        });
        assertEquals(1, clientsRecus.size());

        ClientDto clientRecu = clientsRecus.get(0);
        assertEquals("CLIENT_1", clientRecu.getCode());
        assertEquals("Client 1", clientRecu.getAdresse().getRaisonSociale());
        assertEquals("14000", clientRecu.getAdresse().getCodePostal());
        assertEquals("Caen", clientRecu.getAdresse().getVille());
        assertEquals("1 rue de l'église", clientRecu.getAdresse().getLigne());
    }

    @Test
    public void getClient_ExceptionSiInexistant() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/clients/NOT_EXISTS"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof NotFoundException))
                .andExpect(result -> assertEquals("Le client NOT_EXISTS n'a pas été trouvé.", result.getResolvedException().getMessage()));
    }

    @Test
    public void getClient_ValeurSiExistant() throws Exception {
        // On vérifie son absence en entrée
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/clients/CLIENT_1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof NotFoundException))
                .andExpect(result -> assertEquals("Le client CLIENT_1 n'a pas été trouvé.", result.getResolvedException().getMessage()));

        ClientCreationDto clientCreationDto = new ClientCreationDto();
        clientCreationDto.setCode("CLIENT_1");
        AdresseCreationDto adresseCreationDto = new AdresseCreationDto();
        adresseCreationDto.setRaisonSociale("Client 1");
        adresseCreationDto.setCodePostal("14000");
        adresseCreationDto.setVille("Caen");
        adresseCreationDto.setLigne("1 rue de l'église");
        clientCreationDto.setAdresse(adresseCreationDto);
        this.mockMvc.perform(
                        MockMvcRequestBuilders.post("/api/v1/clients")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(clientCreationDto))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());

        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/clients/CLIENT_1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        ClientDto clientRecu = objectMapper.readValue(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8), ClientDto.class);

        assertEquals("CLIENT_1", clientRecu.getCode());
        assertEquals("Client 1", clientRecu.getAdresse().getRaisonSociale());
        assertEquals("14000", clientRecu.getAdresse().getCodePostal());
        assertEquals("Caen", clientRecu.getAdresse().getVille());
        assertEquals("1 rue de l'église", clientRecu.getAdresse().getLigne());
    }

    @Test
    public void creerClient_CasNominal() throws Exception {
        // On vérifie son absence en entrée
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/clients/CLIENT_1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof NotFoundException))
                .andExpect(result -> assertEquals("Le client CLIENT_1 n'a pas été trouvé.", result.getResolvedException().getMessage()));

        ClientCreationDto clientCreationDto = new ClientCreationDto();
        clientCreationDto.setCode("CLIENT_1");
        AdresseCreationDto adresseCreationDto = new AdresseCreationDto();
        adresseCreationDto.setRaisonSociale("Client 1");
        adresseCreationDto.setCodePostal("14000");
        adresseCreationDto.setVille("Caen");
        adresseCreationDto.setLigne("1 rue de l'église");
        clientCreationDto.setAdresse(adresseCreationDto);
        MvcResult mvcResult = this.mockMvc.perform(
                        MockMvcRequestBuilders.post("/api/v1/clients")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(clientCreationDto))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        ClientDto clientRecu = objectMapper.readValue(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8), ClientDto.class);

        assertEquals("CLIENT_1", clientRecu.getCode());
        assertEquals("Client 1", clientRecu.getAdresse().getRaisonSociale());
        assertEquals("14000", clientRecu.getAdresse().getCodePostal());
        assertEquals("Caen", clientRecu.getAdresse().getVille());
        assertEquals("1 rue de l'église", clientRecu.getAdresse().getLigne());
    }

    @Test
    public void creerClient_DejaExistant() throws Exception {
        // On vérifie son absence en entrée
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/clients/CLIENT_1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof NotFoundException))
                .andExpect(result -> assertEquals("Le client CLIENT_1 n'a pas été trouvé.", result.getResolvedException().getMessage()));

        ClientCreationDto clientCreationDto = new ClientCreationDto();
        clientCreationDto.setCode("CLIENT_1");
        AdresseCreationDto adresseCreationDto = new AdresseCreationDto();
        adresseCreationDto.setRaisonSociale("Client 1");
        adresseCreationDto.setCodePostal("14000");
        adresseCreationDto.setVille("Caen");
        adresseCreationDto.setLigne("1 rue de l'église");
        clientCreationDto.setAdresse(adresseCreationDto);
        this.mockMvc.perform(
                        MockMvcRequestBuilders.post("/api/v1/clients")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(clientCreationDto))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());

        // 2nde création indentique
        this.mockMvc.perform(
                        MockMvcRequestBuilders.post("/api/v1/clients")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(clientCreationDto))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isConflict())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof FunctionalException))
                .andExpect(result -> assertEquals("Le client CLIENT_1 existe déjà.", result.getResolvedException().getMessage()));
    }

    @Test
    public void creerClient_DonneesInvalides() throws Exception {
        // On vérifie son absence en entrée
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/clients/CLIENT_1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof NotFoundException))
                .andExpect(result -> assertEquals("Le client CLIENT_1 n'a pas été trouvé.", result.getResolvedException().getMessage()));

        ClientCreationDto clientCreationDto = new ClientCreationDto();
        clientCreationDto.setCode("CLIENT_1");
        AdresseCreationDto adresseCreationDto = new AdresseCreationDto();
        adresseCreationDto.setRaisonSociale(null); // Ce champ est obligatoire
        adresseCreationDto.setCodePostal("140000000000"); // Ce champ est trop long
        adresseCreationDto.setVille("Caen");
        adresseCreationDto.setLigne("1 rue de l'église");
        clientCreationDto.setAdresse(adresseCreationDto);

        Map<String, String> erreursAttendues = Map.of("adresse.codePostal", "size must be between 0 and 10", "adresse.raisonSociale", "must not be empty");

        this.mockMvc.perform(
                        MockMvcRequestBuilders.post("/api/v1/clients")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(clientCreationDto))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException))
                .andExpect(result -> assertEquals(erreursAttendues, objectMapper.readValue(result.getResponse().getContentAsString(StandardCharsets.UTF_8), new TypeReference<Map<String, String>>() {
                })));
    }


    @Test
    public void modifierAdresseClient_CasNominal() throws Exception {
        // On vérifie son absence en entrée
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/clients/CLIENT_1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof NotFoundException))
                .andExpect(result -> assertEquals("Le client CLIENT_1 n'a pas été trouvé.", result.getResolvedException().getMessage()));

        ClientCreationDto clientCreationDto = new ClientCreationDto();
        clientCreationDto.setCode("CLIENT_1");
        AdresseCreationDto adresseCreationDto = new AdresseCreationDto();
        adresseCreationDto.setRaisonSociale("Client 1");
        adresseCreationDto.setCodePostal("14000");
        adresseCreationDto.setVille("Caen");
        adresseCreationDto.setLigne("1 rue de l'église");
        clientCreationDto.setAdresse(adresseCreationDto);
        this.mockMvc.perform(
                        MockMvcRequestBuilders.post("/api/v1/clients")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(clientCreationDto))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());

        AdresseModificationDto adresseModificationDto = new AdresseModificationDto();
        adresseModificationDto.setRaisonSociale("Client 1");
        adresseModificationDto.setCodePostal("14000");
        adresseModificationDto.setVille("Caen");
        adresseModificationDto.setLigne("1 bis rue de l'église");

        // 2nde création indentique
        MvcResult mvcResult = this.mockMvc.perform(
                        MockMvcRequestBuilders.patch("/api/v1/clients/CLIENT_1/adresses/ACTUELLE")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(adresseModificationDto))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        AdresseDto adresseRecue = objectMapper.readValue(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8), AdresseDto.class);
        assertEquals("Client 1", adresseRecue.getRaisonSociale());
        assertEquals("14000", adresseRecue.getCodePostal());
        assertEquals("Caen", adresseRecue.getVille());
        assertEquals("1 bis rue de l'église", adresseRecue.getLigne());
    }
}
